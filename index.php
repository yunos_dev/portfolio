<?php
/**
 * Created by PhpStorm.
 * User: yunos
 * Date: 2018-02-22
 * Time: 오후 8:27
 */
?>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Front-end Web Developer | Park Gyu-Ae</title>
    <link rel="shortcut icon" href="./images/favicon.ico" type="image/ico">
    <link href="https://fonts.googleapis.com/css?family=Muli:200,300,400,600,700" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script>
        document.createElement('header');
        document.createElement('nav');
        document.createElement('section');
        document.createElement('figure');
        document.createElement('article');
        document.createElement('aside');
        document.createElement('footer');
    </script>
    <![endif]-->
    <link rel="stylesheet" href="./css/reset.css">
    <link rel="stylesheet" href="./css/base.css">
    <link rel="stylesheet" href="./css/layout.css">
    <link rel="stylesheet" href="./css/main.css">
</head>
<body>
<div id="wrapper">
    <header id="header">
        <div class="headerWrap">
            <div class="headerImg"><img src="./images/dog.png" alt=""></div>
            <nav class="gnb">
                <ul>
                    <li><a href="#home">Home</a></li>
                    <li><a href="#about">About me</a></li>
                    <li><a href="#works">Works</a></li>
                    <li><a href="#contact">Contact</a></li>
                </ul>
            </nav>
        </div>
    </header>
    <div id="container">
        <!--   home Section     -->
        <section id="home">
            <div class="myPic"><img src="./images/me2.jpg" alt=""></div>
            <div class="myDesc">
                <h1><span>Hello, World.</span> My name is Park Gyu-Ae<br> I'm a Front-end & PHP<br> Web Developer.</h1>
                <a class="homeBtn" href="#contact">Contact Me</a>
            </div>
        </section>
        <!--   about Section     -->
        <section id="about">
            <div class="aboutWrap">
                <h2>A Little Bit Of My Story</h2>
                <p class="aboutDesc">
                    <strong>Front-end & PHP Web Developer at http://yunos82.cafe24.com/portfolio/</strong>
                    술보다 커피를 사랑하는 웹 프로그래머 박규애입니다.<br>
                    논리적이고 시멘틱한 코드를 지향하며, 효율적이고 이해하기 쉬운 코드를 만들고자 노력하고 있습니다.
                </p>
            </div>
            <aside class="myCats">
                <h2>Introduce My Cats</h2>
            </aside>
        </section>
        <!--   skill Section     -->
        <section id="skill">
            <div class="skillWrap">
                <h2>Expertise</h2>
                <p class="skillDesc">다양한 분야에 관심이 많으며 Full-Stack 개발자가 목표입니다. 현재 한양사이버대학교 컴퓨터공학과(복수전공 해킹보안학과) 재학중입니다.</p>
                <p>SKILLS : Web Standard / Web Accessibility / JavaScript / jQuery / HTML5 / CSS3 <span>(+)</span></p>
            </div>
            <aside class="skillGraph"></aside>
        </section>
        <!--   works Section     -->
        <section id="works">
            <h2>Seleted Works</h2>
            <p>Check this out!</p>
            <div class="worksWrap"></div>
        </section>
        <!--   contact Section     -->
        <section id="contact">
            <h2>Contact By Email</h2>
            <p>Feel Free to Contact Me</p>
            <div class="emailWrap">
                <form id="emailForm" name="emailForm">
                    <fieldset>
                        <legend class="hid">Contact Me</legend>
                        <p>
                            <input type="text" name="name" id="name" title="이름 입력란" placeholder="Name" value="" />
                            <label class="hid" for="name">name</label>
                        </p>
                        <p>
                            <input type="text" name="email" id="email" title="이메일 입력란" placeholder="Email" value="" />
                            <label class="hid" for="email">email</label>
                        </p>
                        <p>
                            <input type="text" name="subject" id="subject" title="제목 입력란" placeholder="Subject" value="" />
                            <label class="hid" for="subject">subject</label>
                        </p>
                        <p>
                            <textarea type="text" name="message" id="message" title="내용 입력란" placeholder="Message" value=""></textarea>
                            <label class="hid" for="message">message</label>
                        </p>
                    </fieldset>
                </form>
            </div>
        </section>
    </div>
    <footer>
        <p><a href="http://yunos82.cafe24.com/dev/" title="Front-end & PHP Web Developer | Park Gyu-Ae">Front-end & PHP Web Developer | Park Gyu-Ae</a></p>
        <p><small>&copy;2018 Park Gyu-Ae portfolio. All Rights Reserved.</small></p>
    </footer>
</div>
</body>
</html>
